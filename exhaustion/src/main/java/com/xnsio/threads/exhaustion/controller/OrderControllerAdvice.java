package com.xnsio.threads.exhaustion.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class OrderControllerAdvice {
    @ExceptionHandler
    ResponseEntity<StackTraceElement[]> handleExceptions(Exception e) {
        log.error("Error in API", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getStackTrace());
    }
}
