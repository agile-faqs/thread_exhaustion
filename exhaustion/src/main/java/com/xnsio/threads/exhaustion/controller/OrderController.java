package com.xnsio.threads.exhaustion.controller;

import com.xnsio.threads.exhaustion.model.Order;
import com.xnsio.threads.exhaustion.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Slf4j
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/api/v1/orders/order")
    public ResponseEntity<String> createOrder(@RequestBody Order order) throws IOException {
        return new ResponseEntity<>(orderService.createOrder(order), HttpStatus.CREATED);
    }

    @GetMapping("/api/v1/orders/order/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable String id) throws IOException {
        return new ResponseEntity<>(orderService.fetchOrder(id), HttpStatus.OK);
    }

    @GetMapping("/api/v1/orders/")
    public ResponseEntity<java.util.List<Order>> fetchOrders() throws IOException {
        return new ResponseEntity<>(orderService.fetchOrders(), HttpStatus.OK);
    }

}
