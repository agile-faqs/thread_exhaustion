package com.xnsio.threads.exhaustion.model;

import lombok.Data;

@Data
public class Item {
    private String sku;
    private Integer quantity;
}
