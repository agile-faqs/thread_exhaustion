package com.xnsio.threads.exhaustion.model;

import lombok.Data;

@Data
public class Order {
    private String id;
    private Cart cart;
}
