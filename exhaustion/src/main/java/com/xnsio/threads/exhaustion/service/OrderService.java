package com.xnsio.threads.exhaustion.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xnsio.threads.exhaustion.Constants;
import com.xnsio.threads.exhaustion.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.xnsio.threads.exhaustion.Constants.INDEX;
import static com.xnsio.threads.exhaustion.Constants.TYPE;

@Slf4j
@Service
public class OrderService {
    private String elasticSearchHostname;
    private int elasticSearchPort;

    private RestHighLevelClient client;
    private ObjectMapper objectMapper;

    @Autowired
    public OrderService(@Value("${elasticsearch.host:127.0.0.1}")String elasticSearchHostname, @Value("${elasticsearch.port:9200}")
    int elasticSearchPort, ObjectMapper objectMapper) {
        this.elasticSearchHostname = elasticSearchHostname;
        this.elasticSearchPort = elasticSearchPort;
        this.client = createClient();
        this.objectMapper = objectMapper;
    }

    @PreDestroy
    public void destroy() throws IOException {
        client.close();
    }

    public String createOrder(Order order) throws IOException {
        UUID uuid = UUID.randomUUID();
        order.setId(uuid.toString());
        order.getCart().setId(uuid.toString());
        IndexRequest indexRequest = new IndexRequest(Constants.INDEX, TYPE, order.getId())
                .source(objectMapper.convertValue(order, Map.class), XContentType.JSON);

        IndexResponse indexResponse =
                client
//                        createClient()
                .index(indexRequest, RequestOptions.DEFAULT);
        return indexResponse.getId();
    }

    public Order fetchOrder(String id) throws IOException {
        GetRequest getRequest = new GetRequest(INDEX, TYPE, id);

        GetResponse getResponse =
                client
//                createClient()
                .get(getRequest, RequestOptions.DEFAULT);
        Map<String, Object> resultMap = getResponse.getSource();
        return objectMapper.convertValue(resultMap, Order.class);
    }

    public List<Order> fetchOrders() throws IOException {
        SearchRequest searchRequest = buildSearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse =
                client
//                createClient()
                        .search(searchRequest, RequestOptions.DEFAULT);

        return getSearchResult(searchResponse);
    }

    private SearchRequest buildSearchRequest() {

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(INDEX);
        searchRequest.types(TYPE);

        return searchRequest;
    }

    private List<Order> getSearchResult(SearchResponse response) {

        SearchHit[] searchHit = response.getHits().getHits();

        List<Order> profileDocuments = new ArrayList<>();

        for (SearchHit hit : searchHit){
            profileDocuments
                    .add(objectMapper
                            .convertValue(hit
                                    .getSourceAsMap(), Order.class));
        }

        return profileDocuments;
    }

    private RestHighLevelClient createClient() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost(elasticSearchHostname, elasticSearchPort, "http")));
    }
}
