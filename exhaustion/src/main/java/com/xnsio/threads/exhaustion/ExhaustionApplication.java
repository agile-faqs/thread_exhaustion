package com.xnsio.threads.exhaustion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExhaustionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExhaustionApplication.class, args);
	}

}
