package com.xnsio.threads.exhaustion.model;

import lombok.Data;

import java.util.List;

@Data
public class Cart {
    private String id;
    private Double amount;
    private List<Item> items;
}