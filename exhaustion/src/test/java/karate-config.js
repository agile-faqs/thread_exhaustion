function fn() {
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);
    var port = 8102;
    var protocol = 'http';
    var config = {baseUrl: protocol + '://127.0.0.1:' + port};
    return config;
}