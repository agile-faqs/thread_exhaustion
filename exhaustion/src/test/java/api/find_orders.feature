@rest
Feature: Find all orders
  Background:
    * url baseUrl
    * configure headers = { Content-Type: 'application/json' }
  Scenario: Create a new cart
    Given path '/api/v1/orders/'
    When method get
    Then status 200