package api;

import com.intuit.karate.junit5.Karate;

class CartRunner {
    @Karate.Test
    Karate addCart() {
        return new Karate().feature("add_cart").relativeTo(getClass());
    }

    @Karate.Test
    Karate fetchOrders() {
        return new Karate().feature("find_orders").relativeTo(getClass());
    }
}
