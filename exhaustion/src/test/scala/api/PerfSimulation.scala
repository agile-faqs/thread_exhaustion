package api

import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import scala.concurrent.duration._

class PerfSimulation extends Simulation {

  val protocol = karateProtocol(
    "/orders/order/{id}" -> Nil,
    "/orders" ->  Nil // pauseFor("get" -> 15, "post" -> 25)
  )

  protocol.nameResolver = (req, ctx) => req.getHeader("karate-name")

  val create = scenario("create").exec(karateFeature("classpath:api/add_cart.feature"))
  val findAll = scenario("find").exec(karateFeature("classpath:api/find_orders.feature"))
//  val find = scenario("delete").exec(karateFeature("classpath:api/find_orders.feature@name=find"))
//  val custom = scenario("custom").exec(karateFeature("classpath:api/custom-rpc.feature"))

  setUp(
    create.inject(rampUsers(30) during (30 seconds)).protocols(protocol),
    findAll.inject(rampUsers(100) during (30 seconds)).protocols(protocol)
//    custom.inject(rampUsers(10) during (5 seconds)).protocols(protocol)
  )

}