@rest
Feature: Add an order
  Background:
    * url baseUrl
    * configure headers = { Content-Type: 'application/json' }
  Scenario: Create a new cart
    Given path '/api/v1/orders/order'
    And request { id: "dummy", cart: { amount: 100, items: [ { sku: "1", quantity: 1} ] }}
    When method post
    Then status 201
