# Getting Started

The app stores orders into an ElasticSearch node & retrieves them via a find all query

To package the app as a docker container, run the maven build:
`mvn package`

This creates a docker image with the label `xnsio:threads`

Next, bring up the app & the ES node: `docker-compose up`

Finally, run the perf test: `mvn clean test-compile gatling:test`
