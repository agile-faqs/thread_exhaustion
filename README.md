# thread_exhaustion

POC to replicate thread exhaustion due to initializing 
a new ElasticSearch RestHighLevelClient on every request.

Links:

https://github.com/AsyncHttpClient/async-http-client/issues/1658

https://discuss.elastic.co/t/java-rest-client-i-o-dispatcher-threads-buildup-in-tomcat/108135

### Getting Started

The Spring Boot app stores orders into an ElasticSearch node & retrieves them via a find all query

Run the following steps inside the exhaustion directory

To package the app as a docker container, run the maven build:
`mvn package`

This creates a docker image with the label `xnsio:threads`

Next, bring up the app & the ES node: `docker-compose up`

Finally, run the perf test: `mvn clean test-compile gatling:test`
